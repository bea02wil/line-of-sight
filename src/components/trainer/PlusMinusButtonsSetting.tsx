import { FC } from "react";
import classes from "../../App.module.scss";
import { ISettingCardProps } from "../../store/types/settingCardTypes";

const PlusMinusButtonsSetting: FC<ISettingCardProps> = ({
  title,
  value,
  onMinusBtnClick,
  onPlusBtnClick,
}) => {
  const titleClasses = [classes.title, classes.titleNonCapitalise];

  return (
    <div className={classes.wordSettingContainer}>
      <div className={classes.titleContainer}>
        <div className={classes.title}>{title}</div>
        <input
          type="text"
          value={value}
          className={classes.speedCardInput}
          autoComplete="off"
          disabled
        />
        <div className={titleClasses.join(" ")}>сек.</div>
      </div>
      <div className={classes.plusMinusBtnsContainer}>
        <button className={classes.speedHandlerBtn} onClick={onMinusBtnClick}>
          <i className="fas fa-minus"></i>
        </button>
        <button className={classes.speedHandlerBtn} onClick={onPlusBtnClick}>
          <i className="fas fa-plus"></i>
        </button>
      </div>
    </div>
  );
};

export default PlusMinusButtonsSetting;
