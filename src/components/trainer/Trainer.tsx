import React, { FC } from "react";
import classes from "../../App.module.scss";
import SettingCard from "./SettingCard";
import { SettingType, ITrainerProps } from "../../store/types/trainerTypes";
import {
  SettingCardTitle,
  ISettingCardProps,
} from "../../store/types/settingCardTypes";
import StartButton from "../startButton/StartButton";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import { useActions } from "../../hooks/useActions";
import { useHistory } from "react-router-dom";

const Trainer: FC<ITrainerProps> = ({ name }) => {
  const {
    wordCount,
    startingDistance,
    wordLetters,
    increasingDistance,
    speed,
  } = useTypedSelector((state) => state.wordSetting);

  const {
    setWordCount,
    setIncreaseSpeed,
    setDecreaseSpeed,
    setStartingDistance,
    setWordLetters,
    setIncreaseDistance,
  } = useActions();

  const history = useHistory();

  const settingCards: ISettingCardProps[] = [
    {
      type: SettingType.slider,
      title: SettingCardTitle.WordCount,
      value: wordCount,
      minScaleValue: 1,
      maxScaleValue: 10,
      step: 1,
      onChange: (e: React.ChangeEvent<HTMLInputElement>) =>
        setWordCount(e.target.value),
    },
    {
      type: SettingType.slider,
      title: SettingCardTitle.StartingDistance,
      value: startingDistance,
      minScaleValue: 5,
      maxScaleValue: 40,
      step: 5,
      onChange: (e: React.ChangeEvent<HTMLInputElement>) =>
        setStartingDistance(e.target.value),
    },
    {
      type: SettingType.slider,
      title: SettingCardTitle.WordLetters,
      value: wordLetters,
      minScaleValue: 3,
      maxScaleValue: 12,
      step: 1,
      onChange: (e: React.ChangeEvent<HTMLInputElement>) =>
        setWordLetters(e.target.value),
    },
    {
      type: SettingType.slider,
      title: SettingCardTitle.IncreasingDistance,
      value: increasingDistance,
      minScaleValue: 5,
      maxScaleValue: 40,
      step: 5,
      onChange: (e: React.ChangeEvent<HTMLInputElement>) =>
        setIncreaseDistance(e.target.value),
    },
    {
      type: SettingType.plusMinusButtons,
      title: SettingCardTitle.Speed,
      value: speed,
      onMinusBtnClick: () => setDecreaseSpeed(),
      onPlusBtnClick: () => setIncreaseSpeed(),
    },
  ];

  const onStartBtnClickHandler = () => {
    history.push("/progress");
  };

  return (
    <div className={classes.trainer}>
      <div className={classes.trainerTitleContainer}>
        <h1 className={classes.trainerTitle}>
          тренажер <span className={classes.quotes}>&#10094;&#10094;</span>
          {name}
          <span className={classes.quotes}>&#10095;&#10095;</span>
        </h1>
      </div>

      {settingCards.map((card) => (
        <SettingCard key={card.title} {...card} />
      ))}

      <div className={classes.startButtonContainer}>
        <StartButton onClick={onStartBtnClickHandler} />
      </div>
    </div>
  );
};

export default Trainer;
