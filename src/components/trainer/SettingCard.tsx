import { FC } from "react";
import { ISettingCardProps } from "../../store/types/settingCardTypes";
import SliderSetting from "./SliderSetting";
import PlusMinusButtonsSetting from "./PlusMinusButtonsSetting";
import { SettingType } from "../../store/types/trainerTypes";

const SettingCard: FC<ISettingCardProps> = (props) => {
  if (props.type === SettingType.slider) {
    return <SliderSetting {...props} />;
  } else {
    return <PlusMinusButtonsSetting {...props} />;
  }
};

export default SettingCard;
