import { FC } from "react";
import classes from "../../App.module.scss";
import { ISettingCardProps } from "../../store/types/settingCardTypes";
import {
  getScaleValues,
  isActiveScaleValue,
} from "../../functions/sliderSettingFunctions";

const SliderSetting: FC<ISettingCardProps> = ({
  minScaleValue,
  maxScaleValue,
  step,
  title,
  value,
  onChange,
}) => {
  const scaleValues = getScaleValues(minScaleValue, maxScaleValue, step);
  const scaleValueClasses = [classes.scaleValue, classes.activeScaleValue];

  return (
    <div className={classes.wordSettingContainer}>
      <div className={classes.title}>{title}</div>
      <div className={classes.sliderContainer}>
        <div className={classes.scaleValues}>
          {scaleValues.map((v) => (
            <div
              key={v}
              className={
                !isActiveScaleValue(v, value)
                  ? classes.scaleValue
                  : scaleValueClasses.join(" ")
              }
            >
              {v}
            </div>
          ))}
        </div>
        <div className={classes.sliderSubContainer}>
          <input
            type="range"
            min={minScaleValue?.toString()}
            max={maxScaleValue?.toString()}
            step={step}
            value={value}
            onChange={onChange}
            className={classes.slider}
          />
        </div>
      </div>
    </div>
  );
};

export default SliderSetting;
