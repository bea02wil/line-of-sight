import { FC, useEffect } from "react";
import classes from "../../App.module.scss";
import niceWork from "../../data/niceWork.jpg";
import { useHistory } from "react-router-dom";

const TrainerFinished: FC = () => {
  const history = useHistory();

  useEffect(() => {
    const timeout = setTimeout(() => {
      history.push("/");
    }, 3000);

    return () => {
      clearTimeout(timeout);
    };

    //eslint-disable-next-line
  }, []);

  return (
    <div className={classes.trainerFinished}>
      <img src={niceWork} alt="Nice Work" />
      <h1 className={classes.trainerFinishedTitle}>Отличная работа!</h1>
    </div>
  );
};

export default TrainerFinished;
