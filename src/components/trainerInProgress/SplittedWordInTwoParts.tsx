import { FC, useState, useEffect } from "react";
import classes from "../../App.module.scss";
import separator from "../../data/separator.svg";
import { ISplittedWordInTwoPartsProps } from "../../store/types/splittedWordInTwoPartsTypes";

const SplittedWordInTwoParts: FC<ISplittedWordInTwoPartsProps> = ({
  firstWordPart,
  secondWordPart,
  separatorStyle,
  deadTime,
}) => {
  const [renderComponent, setRenderComponent] = useState<boolean>(true);

  const deadInterval = deadTime * 1000;

  useEffect(() => {
    const timeout = setTimeout(() => {
      setRenderComponent(false);
    }, deadInterval);

    return () => {
      clearTimeout(timeout);
    };

    //eslint-disable-next-line
  }, []);

  return (
    <>
      {renderComponent && (
        <>
          <h2 className={classes.wordDecor}>{firstWordPart}</h2>
          <img src={separator} alt="separator" style={separatorStyle} />
          <h2 className={classes.wordDecor}>{secondWordPart}</h2>
        </>
      )}
    </>
  );
};

export default SplittedWordInTwoParts;
