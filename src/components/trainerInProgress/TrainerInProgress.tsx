import { FC, useState, useEffect } from "react";
import classes from "../../App.module.scss";
import { ISeparatorStyle } from "../../store/types/separatorStyleTypes";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import { wordBase } from "../../data/data";
import SplittedWordInTwoParts from "./SplittedWordInTwoParts";
import {
  getWordsByLettersCount,
  getCertainNumberOfWords,
  shuffleArray,
  getWordsSplittedInTwoParts,
  startShowWords,
} from "../../functions/trainerInProgressFunctions";
import { IWordsSplittedInTwoParts } from "../../store/types/trainerTypes";
import { useHistory } from "react-router-dom";

const TrainerInProgress: FC = () => {
  const {
    wordCount,
    wordLetters,
    startingDistance,
    increasingDistance,
    speed,
  } = useTypedSelector((state) => state.wordSetting);

  const history = useHistory();

  const [count, setCount] = useState<number>(0);

  let distance = 0;
  let speedIncreasingStep = 0.5;
  let deadTime = +speed;

  useEffect(() => {
    let counter = count;
    let timeout = startShowWords(
      counter,
      savedShuffledSplittedWords,
      count,
      +speed,
      speedIncreasingStep,
      () => setCount((count) => count + 1),
      () => history.push("finished")
    );

    return () => clearTimeout(timeout);

    //eslint-disable-next-line
  }, []);

  const wordsByLettersCount = getWordsByLettersCount(wordBase, +wordLetters);
  const shuffledWords = shuffleArray(wordsByLettersCount);

  const shuffledWordsToShow = getCertainNumberOfWords(
    shuffledWords,
    +wordCount
  );

  const splittedWords = getWordsSplittedInTwoParts(shuffledWordsToShow);

  const [savedShuffledSplittedWords] =
    useState<IWordsSplittedInTwoParts[]>(splittedWords);

  return (
    <div className={classes.trainerInProgress}>
      {savedShuffledSplittedWords.slice(0, count).map((word, index) => {
        distance += +increasingDistance;
        deadTime = index === 0 ? deadTime : deadTime + speedIncreasingStep;
        deadTime = Math.min(5, deadTime);

        const separatorStyle: ISeparatorStyle = {
          color: `#371548`,
          margin: `0 calc(${startingDistance}% + ${
            index !== 0 ? distance.toString() : "0"
          }px)`,
        };

        return (
          <SplittedWordInTwoParts
            key={index}
            firstWordPart={word.firstWordPart}
            secondWordPart={word.secondWordPart}
            separatorStyle={separatorStyle}
            deadTime={deadTime}
          />
        );
      })}
    </div>
  );
};

export default TrainerInProgress;
