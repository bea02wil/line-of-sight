import { FC } from "react";
import classes from "../../App.module.scss";
import { IStartButtonProps } from "../../store/types/startButtonTypes";

const StartButton: FC<IStartButtonProps> = ({ onClick }) => {
  const startButtonClasses = [classes.startButton, classes.title];

  return (
    <button className={startButtonClasses.join(" ")} onClick={onClick}>
      старт
    </button>
  );
};

export default StartButton;
