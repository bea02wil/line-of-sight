import { IWordsSplittedInTwoParts } from "../store/types/trainerTypes";

export const getWordsByLettersCount = (
  words: string[],
  wordLetters: number
): string[] => {
  const wordsByLettersCount = words.filter(
    (word) => word.length === wordLetters
  );

  return wordsByLettersCount;
};

export const shuffleArray = <T>(arr: T[]): T[] => {
  const shuffledArray = arr
    .map((value) => ({ value, sort: Math.random() }))
    .sort((a, b) => a.sort - b.sort)
    .map(({ value }) => value);

  return shuffledArray;
};

export const getCertainNumberOfWords = (
  words: string[],
  wordCount: number
): string[] => {
  const certainNumberOfWords: string[] = [];

  for (let i = 0; i < wordCount; i++) {
    certainNumberOfWords.push(words[i]);
  }

  return certainNumberOfWords;
};

const splitWordInTwoParts = (word: string): IWordsSplittedInTwoParts => {
  let firstWordPart: string = "";
  let secondWordPart: string = "";

  const firstWordPartEndIndex = Math.trunc(word.length / 2);
  const secondWordPartFirstIndex = firstWordPartEndIndex;

  for (let i = 0; i < firstWordPartEndIndex; i++) {
    firstWordPart += word.charAt(i);
  }

  for (let i = secondWordPartFirstIndex; i < word.length; i++) {
    secondWordPart += word.charAt(i);
  }

  return {
    firstWordPart,
    secondWordPart,
  };
};

export const getWordsSplittedInTwoParts = (
  words: string[]
): IWordsSplittedInTwoParts[] => {
  const splittedWords = words.map((word) => {
    return {
      firstWordPart: splitWordInTwoParts(word).firstWordPart,
      secondWordPart: splitWordInTwoParts(word).secondWordPart,
    };
  });

  return splittedWords;
};

export const startShowWords = (
  counter: number,
  words: IWordsSplittedInTwoParts[],
  count: number,
  speed: number,
  speedIncreasingStep: number,
  setCount: () => void,
  onEnd: () => void
) => {
  let timeout = setTimeout(() => {
    if (counter >= words.length) {
      clearTimeout(timeout);
      onEnd();
    } else {
      setCount();
      speed = counter === 0 ? speed : speed + speedIncreasingStep;
      speed = Math.min(5, +speed);
      counter++;

      timeout = setTimeout(
        () =>
          startShowWords(
            counter,
            words,
            count,
            speed,
            speedIncreasingStep,
            setCount,
            onEnd
          ),
        (+speed * 1000) / 2
      );
    }
  }, (+speed * 1000) / 2);

  return timeout;
};
