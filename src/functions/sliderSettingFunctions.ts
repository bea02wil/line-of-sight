export const getScaleValues = (
  minScaleValue?: number,
  maxScaleValue?: number,
  step?: number
): string[] => {
  const values: string[] = [];

  for (let i = minScaleValue!; i <= maxScaleValue!; i += step!) {
    values.push(i.toString());
  }

  return values;
};

export const isActiveScaleValue = (
  scaleValue: string,
  currentValue: string
): boolean => {
  return scaleValue === currentValue;
};
