//import "./App.module.scss";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Trainer from "./components/trainer/Trainer";
import TrainerInProgress from "./components/trainerInProgress/TrainerInProgress";
import TrainerFinished from "./components/trainerFinished/TrainerFinished";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route
          exact
          path="/"
          component={() => <Trainer name={"Поле зрения"} />}
        ></Route>
        <Route exact path="/progress" component={TrainerInProgress}></Route>
        <Route exact path="/finished" component={TrainerFinished}></Route>
      </Switch>
    </Router>
  );
};

export default App;
