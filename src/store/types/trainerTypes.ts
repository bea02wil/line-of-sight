export interface IWordsSplittedInTwoParts {
  firstWordPart: string;
  secondWordPart: string;
}

export enum SettingType {
  slider = "slider",
  plusMinusButtons = "plusMinusButtons",
}

export interface ITrainerProps {
  name: string;
}
