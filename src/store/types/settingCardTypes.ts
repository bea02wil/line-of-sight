export interface ISettingCardState {
  wordCount: string;
  startingDistance: string;
  wordLetters: string;
  increasingDistance: string;
  speed: string;
}

export interface ISettingCardProps {
  type: string;
  title: string;
  value: string;
  minScaleValue?: number;
  maxScaleValue?: number;
  step?: number;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onMinusBtnClick?: () => void;
  onPlusBtnClick?: () => void;
}

export enum SettingCardActionTypes {
  SET_WORD_COUNT = "SET_WORD_COUNT",
  SET_STARTING_DISTANCE = "SET_STARTING_DISTANCE",
  SET_WORD_LETTERS = "SET_WORD_LETTERS",
  SET_INCREASE_DISTANCE = "SET_INCREASE_DISTANCE",
  SET_INCREASE_SPEED = "SET_INCREASE_SPEED",
  SET_DECREASE_SPEED = "SET_DECREASE_SPEED",
}

export enum SpeedMinMaxValues {
  MinSpeed = 1,
  MaxSpeed = 5,
}

export enum SettingCardTitle {
  WordCount = "сколько слов",
  StartingDistance = "стартовое расстояние",
  WordLetters = "сколько букв в словах",
  IncreasingDistance = "увеличение расстояния",
  Speed = "скорость",
}

interface IPayload {
  payload: string;
}

interface ISetWordCountAction extends IPayload {
  type: SettingCardActionTypes.SET_WORD_COUNT;
}

interface ISetStartingDistanceAction extends IPayload {
  type: SettingCardActionTypes.SET_STARTING_DISTANCE;
}

interface ISetWordLettersAction extends IPayload {
  type: SettingCardActionTypes.SET_WORD_LETTERS;
}

interface ISetIncreaseDistanceAction extends IPayload {
  type: SettingCardActionTypes.SET_INCREASE_DISTANCE;
}

interface ISetIncreaseSpeedAction {
  type: SettingCardActionTypes.SET_INCREASE_SPEED;
}

interface ISetDecreaseSpeedAction {
  type: SettingCardActionTypes.SET_DECREASE_SPEED;
}

export type WordSettingCardAction =
  | ISetWordCountAction
  | ISetStartingDistanceAction
  | ISetWordLettersAction
  | ISetIncreaseDistanceAction
  | ISetIncreaseSpeedAction
  | ISetDecreaseSpeedAction;
