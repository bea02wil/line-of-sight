export interface ISeparatorStyle {
  color: string;
  margin: string;
}
