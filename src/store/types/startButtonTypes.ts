export interface IStartButtonProps {
  onClick: () => void;
}
