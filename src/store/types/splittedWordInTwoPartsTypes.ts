import { IWordsSplittedInTwoParts } from "./trainerTypes";
import { ISeparatorStyle } from "./separatorStyleTypes";

export interface ISplittedWordInTwoPartsProps extends IWordsSplittedInTwoParts {
  separatorStyle: ISeparatorStyle;
  deadTime: number;
}
