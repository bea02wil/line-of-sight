import { combineReducers } from "redux";
import { settingReducer } from "./settingReducer";

export const rootReducer = combineReducers({
  wordSetting: settingReducer,
});

export type RootReducer = ReturnType<typeof rootReducer>;
