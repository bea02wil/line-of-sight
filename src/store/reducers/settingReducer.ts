import {
  ISettingCardState,
  SpeedMinMaxValues,
  SettingCardActionTypes,
} from "../types/settingCardTypes";
import { WordSettingCardAction } from "../types/settingCardTypes";

const initialState: ISettingCardState = {
  wordCount: "6",
  startingDistance: "25",
  wordLetters: "9",
  increasingDistance: "25",
  speed: "1",
};

export const settingReducer = (
  state = initialState,
  action: WordSettingCardAction
): ISettingCardState => {
  switch (action.type) {
    case SettingCardActionTypes.SET_WORD_COUNT:
      return {
        ...state,
        wordCount: action.payload,
      };
    case SettingCardActionTypes.SET_INCREASE_SPEED:
      return {
        ...state,
        speed: Math.min(
          SpeedMinMaxValues.MaxSpeed,
          +state.speed + 1
        ).toString(),
      };
    case SettingCardActionTypes.SET_DECREASE_SPEED:
      return {
        ...state,
        speed: Math.max(
          SpeedMinMaxValues.MinSpeed,
          +state.speed - 1
        ).toString(),
      };
    case SettingCardActionTypes.SET_STARTING_DISTANCE:
      return {
        ...state,
        startingDistance: action.payload,
      };
    case SettingCardActionTypes.SET_WORD_LETTERS:
      return {
        ...state,
        wordLetters: action.payload,
      };
    case SettingCardActionTypes.SET_INCREASE_DISTANCE:
      return {
        ...state,
        increasingDistance: action.payload,
      };
    default:
      return state;
  }
};
