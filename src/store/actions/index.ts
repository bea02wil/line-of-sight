import * as SettingCardActions from "./settingCardActions";

const Actions = {
  ...SettingCardActions,
};

export default Actions;
