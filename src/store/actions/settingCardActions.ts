import {
  WordSettingCardAction,
  SettingCardActionTypes,
} from "../types/settingCardTypes";

export const setWordCount = (value: string): WordSettingCardAction => {
  return {
    type: SettingCardActionTypes.SET_WORD_COUNT,
    payload: value,
  };
};

export const setIncreaseSpeed = (): WordSettingCardAction => {
  return {
    type: SettingCardActionTypes.SET_INCREASE_SPEED,
  };
};

export const setDecreaseSpeed = (): WordSettingCardAction => {
  return {
    type: SettingCardActionTypes.SET_DECREASE_SPEED,
  };
};

export const setStartingDistance = (value: string): WordSettingCardAction => {
  return {
    type: SettingCardActionTypes.SET_STARTING_DISTANCE,
    payload: value,
  };
};

export const setWordLetters = (value: string): WordSettingCardAction => {
  return {
    type: SettingCardActionTypes.SET_WORD_LETTERS,
    payload: value,
  };
};

export const setIncreaseDistance = (value: string): WordSettingCardAction => {
  return {
    type: SettingCardActionTypes.SET_INCREASE_DISTANCE,
    payload: value,
  };
};
